package com.voxelbuster.hardcorelivesplugin;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("UnusedReturnValue")
public class ConfigManager {
    private Config config;
    private final HashMap<OfflinePlayer, PlayerData> playerDataMap = new HashMap<>();
    private final HardcoreLivesPlugin plugin;

    public ConfigManager(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean loadConfig(File configFile) throws IOException {
        if (generateConfig(configFile)) {
            return true;
        } else {
            Gson gson = new Gson();
            FileReader fr = new FileReader(configFile);
            try {
                this.config = gson.fromJson(fr, Config.class);
            } catch (JsonSyntaxException e) {
                plugin.getLogger().severe("Bad JSON in config, resetting.");
                e.printStackTrace();
                generateConfig(configFile);
                return false;
            }
            return false;
        }
    }

    public boolean generateConfig(File configFile) throws IOException {
        if (configFile.createNewFile()) {
            FileWriter fw = new FileWriter(configFile);
            this.config = new Config();
            Gson gson = new Gson();
            gson.toJson(config, fw);
            fw.close();
            return true;
        }

        return false;
    }

    public void saveConfig(File configFile) throws IOException {
        FileWriter fw = new FileWriter(configFile);
        Gson gson = new Gson();
        gson.toJson(config, fw);
        fw.close();
    }

    public void setPlayerData(Player p, PlayerData data) {
        this.playerDataMap.put(p, data);
    }

    public void saveAllPlayers(File playersDir) throws IOException {
        for (OfflinePlayer p : playerDataMap.keySet()) {
            savePlayer(p, playersDir);
        }
    }

    public boolean savePlayer(OfflinePlayer p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            gson.toJson(playerDataMap.get(p), fw);
            fw.close();
            return true;
        }
    }

    public void wipePlayers(File playersDir) throws IOException {
        OfflinePlayer[] loadedPlayers = getLoadedPlayers();
        unloadAllPlayers(playersDir);

        Files.move(playersDir.toPath(), Paths.get(playersDir.getParentFile().toPath().toString(), "players.old"),
                StandardCopyOption.REPLACE_EXISTING);
        //noinspection ResultOfMethodCallIgnored
        playersDir.mkdir();
        playerDataMap.clear();

        for (OfflinePlayer p : loadedPlayers) {
            loadPlayerData(p, playersDir);
        }
    }

    public OfflinePlayer[] getLoadedPlayers() {
        return playerDataMap.keySet().toArray(new OfflinePlayer[0]);
    }

    public void unloadAllPlayers(File playersDir) throws IOException {
        for (OfflinePlayer p : playerDataMap.keySet()) {
            unloadPlayer(p, playersDir);
        }
    }

    /**
     * Loads the PlayerData into the internal Map. Returns true if the file was created and false if it already exists.
     *
     * @param p          the player
     * @param playersDir player data storage directory
     * @return true if existing data was loaded, false if a new save had to be generated
     * @throws IOException if a file could not be written
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean loadPlayerData(OfflinePlayer p, File playersDir) throws IOException {
        if (!playersDir.isDirectory()) {
            throw new FileNotFoundException("playersDir must be a directory!");
        } else {
            File playerFile = new File(
                    Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            if (playerFile.createNewFile()) {
                PlayerData playerData = new PlayerData(p, this.getConfig());
                playerDataMap.put(p, playerData);
                return true;
            } else {
                Gson gson = new Gson();
                FileReader fr = new FileReader(playerFile);
                try {
                    PlayerData pd = gson.fromJson(fr, PlayerData.class);
                    playerDataMap.put(p, pd);
                } catch (JsonSyntaxException e) {
                    plugin.getLogger().severe("Bad JSON in config, resetting.");
                    e.printStackTrace();
                    playerDataMap.put(p, new PlayerData(p, this.getConfig()));
                }
                fr.close();
                return false;
            }
        }
    }

    public boolean unloadPlayer(OfflinePlayer p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                    Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            gson.toJson(playerDataMap.remove(p), fw);
            fw.close();
            return true;
        }
    }

    public Config getConfig() {
        return config;
    }

    public PlayerData getPlayerData(OfflinePlayer p) {
        return playerDataMap.get(p);
    }

    public static class Config implements Serializable {
        private int startingLives = 3;
        private float lifeBuyPrice = 10000;
        private float lifeSellPrice = 5000;
        private int autosaveInterval = 5;
        private TimeUnit autosaveUnit = TimeUnit.MINUTES;
        private boolean allowBuyingLives = false;
        private boolean allowSellingLives = false;
        private boolean allowGivingLives = false;
        private boolean allowTotemOfUndying = true;
        private boolean spectateWhenNoMoreLives = true;
        private boolean loseLifeOnPvpOnly = false;
        private DisplaySlot scoreboardDisplaySlot = DisplaySlot.BELOW_NAME;

        public int getStartingLives() {
            return startingLives;
        }

        public void setStartingLives(int startingLives) {
            this.startingLives = startingLives;
        }

        public boolean allowsTotemOfUndying() {
            return allowTotemOfUndying;
        }

        public void setAllowTotemOfUndying(boolean allowTotemOfUndying) {
            this.allowTotemOfUndying = allowTotemOfUndying;
        }

        public boolean canSpectateWhenNoMoreLives() {
            return spectateWhenNoMoreLives;
        }

        public void setSpectateWhenNoMoreLives(boolean spectateWhenNoMoreLives) {
            this.spectateWhenNoMoreLives = spectateWhenNoMoreLives;
        }

        public float getLifeBuyPrice() {
            return lifeBuyPrice;
        }

        public void setLifeBuyPrice(float lifeBuyPrice) {
            this.lifeBuyPrice = lifeBuyPrice;
        }

        public float getLifeSellPrice() {
            return lifeSellPrice;
        }

        public void setLifeSellPrice(float lifeSellPrice) {
            this.lifeSellPrice = lifeSellPrice;
        }

        public boolean allowBuyingLives() {
            return allowBuyingLives;
        }

        public void setAllowBuyingLives(boolean allowBuyingLives) {
            this.allowBuyingLives = allowBuyingLives;
        }

        public boolean allowSellingLives() {
            return allowSellingLives;
        }

        public void setAllowSellingLives(boolean allowSellingLives) {
            this.allowSellingLives = allowSellingLives;
        }

        public boolean allowGivingLives() {
            return allowGivingLives;
        }

        public void setAllowGivingLives(boolean allowGivingLives) {
            this.allowGivingLives = allowGivingLives;
        }

        public DisplaySlot getScoreboardDisplaySlot() {
            return this.scoreboardDisplaySlot;
        }

        public void setScoreboardDisplaySlot(DisplaySlot scoreboardDisplaySlot) {
            this.scoreboardDisplaySlot = scoreboardDisplaySlot;
        }

        public int getAutosaveInterval() {
            return autosaveInterval;
        }

        public void setAutosaveInterval(int autosaveInterval) {
            this.autosaveInterval = autosaveInterval;
        }

        public TimeUnit getAutosaveUnit() {
            return autosaveUnit;
        }

        public void setAutosaveUnit(TimeUnit autosaveUnit) {
            this.autosaveUnit = autosaveUnit;
        }

        public boolean doLoseLifeOnPvpOnly() {
            return loseLifeOnPvpOnly;
        }

        public void setLoseLifeOnPvpOnly(boolean loseLifeOnPvpOnly) {
            this.loseLifeOnPvpOnly = loseLifeOnPvpOnly;
        }
    }

    @SuppressWarnings("unused")
    public static class PlayerData implements Serializable {
        private UUID uuid;
        private String username;
        private int lives;
        private boolean bypassLives;

        private int maxLives;

        public PlayerData(OfflinePlayer p, Config c) {
            this(p, c.getStartingLives(), false, 5);
        }

        public PlayerData(OfflinePlayer p, int lives, boolean bypassLives, int maxLives) {
            this.uuid = p.getUniqueId();
            this.username = p.getName();
            this.bypassLives = bypassLives;
            this.lives = lives;
            this.maxLives = maxLives;
        }

        public UUID getUuid() {
            return uuid;
        }

        public void setUuid(UUID uuid) {
            this.uuid = uuid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getLives() {
            return lives;
        }

        public void setLives(int lives) {
            this.lives = Math.min(lives, this.maxLives);
        }

        public boolean doesBypassLives() {
            return bypassLives;
        }

        public void setBypassLives(boolean bypassLives) {
            this.bypassLives = bypassLives;
        }

        public int getMaxLives() {
            return maxLives;
        }

        public void setMaxLives(int maxLives) {
            this.maxLives = maxLives;
        }
    }
}
