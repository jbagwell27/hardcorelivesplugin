package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SubcommandSetMax extends PluginSubcommand {
    protected SubcommandSetMax(HardcoreLivesPlugin plugin) {
        super("setMax", plugin);
        this.description = "Set the maximum life a player can have.";
        this.usage = "/hl setMax <player> <number>";
        this.aliases = Arrays.asList("setmax", "sm");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length == 2) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.setLives")) {

                Player p = plugin.getServer().getPlayer(args[0]);
                if (p == null) {
                    sender.sendMessage(ChatColor.RED + "Player not found.");
                } else {
                    ConfigManager.PlayerData data = configManager.getPlayerData(p);
                    data.setMaxLives(Integer.parseInt(args[1]));
                    try {
                        plugin.getConfigManager().savePlayer(p, plugin.getPlayersDir());
                    } catch (IOException e) {
                        sender.sendMessage(ChatColor.RED + "Saving player data failed!");
                        plugin.getLogger().severe("Saving player data failed!");
                        e.printStackTrace();
                        return false;
                    }
                    sender.sendMessage(ChatColor.GREEN + "Saving complete.");
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }

        } else {
            sendUsageMessage(sender);
            return false;
        }


    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> playerNames = new ArrayList<>();
                plugin.getServer().getOnlinePlayers().forEach(p -> playerNames.add(p.getName()));
                return playerNames.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
