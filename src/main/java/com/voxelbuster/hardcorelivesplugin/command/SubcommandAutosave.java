package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class SubcommandAutosave extends PluginSubcommand {
    protected SubcommandAutosave(HardcoreLivesPlugin plugin) {
        super("autosave", plugin);
        this.description = """
                Set the autosave interval for player data.
                It is not recommended you set this lower than 10 seconds.
                Setting this to 0 or lower will disable auto saving.
                This only applies while the server is running. Restarting the server will reset the timer.""";
        this.usage = "/hl autosave <number> <time_interval>";
        this.aliases = Collections.singletonList("autosave");

    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "hardcorelives.autosave")) {
            if (aliases.contains(alias.toLowerCase())) {
                if (args.length == 2) {
                    ConfigManager configManager = plugin.getConfigManager();

                    int interval;

                    try {
                        interval = Integer.parseInt(args[0]);
                        configManager.getConfig().setAutosaveInterval(interval);
                    } catch (NumberFormatException e) {
                        sender.sendMessage(ChatColor.RED + "Argument 1 must be an integer!");
                        return false;
                    }

                    TimeUnit unit;

                    try {
                        unit = TimeUnit.valueOf(args[1]);
                        configManager.getConfig().setAutosaveUnit(unit);
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(ChatColor.RED + "Argument 2 must be a time unit!");
                        return false;
                    }

                    plugin.getSaveTaskScheduler().setSaveInterval(unit.toMillis(interval) / 50);
                    plugin.getSaveTaskScheduler().rescheduleTask();

                    sender.sendMessage(ChatColor.GREEN + String
                            .format("Autosave interval set to %d %s.", interval, unit));

                    return true;
                } else {
                    sendUsageMessage(sender);
                    return false;
                }
            }
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
        sendUsageMessage(sender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length > 1) {
                ArrayList<String> unitNames = new ArrayList<>();
                Arrays.asList(TimeUnit.values()).forEach(tu -> unitNames.add(tu.toString()));
                return unitNames.stream().filter(s -> s.startsWith(args[1])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
