package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PluginBaseCommand extends Command implements CommandExecutor, TabCompleter {

    private final HardcoreLivesPlugin plugin;

    List<String> commandAliases = Arrays.asList("hardcorelives", "hl", "hlives");

    public PluginBaseCommand(String name, HardcoreLivesPlugin plugin) {
        super(name);
        this.plugin = plugin;
        this.description = ChatColor.AQUA +
                "Command that contains all functionality of HardcoreLives" + ChatColor.GREEN + " /hl" + ChatColor.AQUA
                + " or" + ChatColor.GREEN + " /hl help to see subcommands.";
        this.usageMessage = ChatColor.GREEN + "/hl <subcommand>";
        this.setAliases(Arrays.asList("hl", "hardcorelives", "hlives"));
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String alias,
            String[] args) {
        return execute(commandSender, alias, args);
    }

    @Override
    public boolean execute(@NotNull CommandSender commandSender, String alias, String[] args) {
        if (commandAliases.contains(alias.toLowerCase())) {
            if (args.length > 0) {
                switch (Objects.requireNonNull(SubCommand.subCommandByAlias(args[0].toLowerCase()))) {
                    case SAVE:
                        return new SubcommandSave(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case LIVES:
                        return new SubcommandLives(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RESET:
                        return new SubcommandReset(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case CONFIG:
                        return new SubcommandConfig(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RELOAD:
                        return new SubcommandReload(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case AUTOSAVE:
                        return new SubcommandAutosave(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RESETALL:
                        return new SubcommandResetAll(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SETLIVES:
                        return new SubcommandSetLives(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case ADDLIVES:
                        return new SubcommandAddLives(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SETMAX:
                        return new SubcommandSetMax(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case GIVELIVES:
                        return new SubcommandGiveLives(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SCOREBOARD:
                        return new SubcommandScoreboard(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SCOREBOARDSLOT:
                        return new SubcommandScoreboardSlot(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case NONE:
                        return new SubcommandHelp(plugin).execute(commandSender, "help", new String[0]);
                    case HELP:
                        return new SubcommandHelp(plugin)
                                .execute(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    default:
                        commandSender.sendMessage(ChatColor.RED + "Invalid subcommand. " + ChatColor.GREEN +
                                "/hl help " + ChatColor.RED + "for help.");
                        return false;
                }
            } else {
                return new SubcommandHelp(plugin).execute(commandSender, "help", new String[0]);
            }
        }
        return false;
    }

    @Override
    public @NotNull List<String> tabComplete(@NotNull CommandSender commandSender, String alias,
            String[] args) throws IllegalArgumentException {
        if (commandAliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                return SubCommand.getAllAliases().stream().filter(s -> s.startsWith(args[0].toLowerCase()))
                        .collect(Collectors.toList());
            } else {
                return switch (Objects.requireNonNull(SubCommand.subCommandByAlias(args[0].toLowerCase()))) {
                    case SAVE -> new SubcommandSave(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case LIVES -> new SubcommandLives(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RESET -> new SubcommandReset(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case CONFIG -> new SubcommandConfig(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RELOAD -> new SubcommandReload(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case AUTOSAVE -> new SubcommandAutosave(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case RESETALL -> new SubcommandResetAll(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SETLIVES -> new SubcommandSetLives(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case ADDLIVES -> new SubcommandSetMax(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SETMAX -> new SubcommandSetMax(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case GIVELIVES -> new SubcommandGiveLives(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SCOREBOARD -> new SubcommandScoreboard(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case SCOREBOARDSLOT -> new SubcommandScoreboardSlot(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                    case NONE -> Collections.emptyList();
                    case HELP -> new SubcommandHelp(plugin)
                            .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
                };
            }
        } else {
            return super.tabComplete(commandSender, alias, args);
        }
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command,
            @NotNull String alias, String[] args) {
        return tabComplete(commandSender, alias, args);
    }

    enum SubCommand {
        NONE(SubcommandHelp.class, "", null),
        HELP(SubcommandHelp.class, "help", "h"),
        LIVES(SubcommandLives.class, "lives", "l"),
        RESET(SubcommandReset.class, "reset"),
        RESETALL(SubcommandResetAll.class, "resetall"),
        RELOAD(SubcommandReload.class, "reload"),
        CONFIG(SubcommandConfig.class, "config", "c"),
        SETLIVES(SubcommandSetLives.class, "setlives", "sl"),
        ADDLIVES(SubcommandAddLives.class, "addlives", "al"),
        SETMAX(SubcommandSetMax.class, "setmax", "sm"),
        GIVELIVES(SubcommandGiveLives.class, "givelives", "gl"),
        SCOREBOARDSLOT(SubcommandScoreboardSlot.class, "scoreboardslot", "ss"),
        SCOREBOARD(SubcommandScoreboard.class, "scoreboard", "scoreboarddisplay", "sd"),
        SAVE(SubcommandSave.class, "save"),
        AUTOSAVE(SubcommandAutosave.class, "autosave");

        private final List<String> aliases;

        private final Class<? extends PluginSubcommand> pluginSubcommandClass;

        SubCommand(Class<? extends PluginSubcommand> pluginSubcommandClass,
                String... aliases) {
            this.pluginSubcommandClass = pluginSubcommandClass;
            this.aliases = Arrays.asList(aliases);
        }

        public static SubCommand subCommandByAlias(String s) {
            for (SubCommand subCommand : values()) {
                if (subCommand.hasAlias(s)) {
                    return subCommand;
                }
            }
            return NONE;
        }

        public boolean hasAlias(String s) {
            return this.aliases.contains(s);
        }

        public static ArrayList<String> getAllAliases() {
            ArrayList<String> aliases = new ArrayList<>();
            for (SubCommand subCommand : values()) {
                aliases.addAll(subCommand.aliases.stream().filter(s -> s != null && !s.isEmpty())
                        .collect(Collectors.toCollection(ArrayList::new)));
            }
            return aliases;
        }

        public Class<? extends PluginSubcommand> getSubcommand() {
            return pluginSubcommandClass;
        }
    }
}
