package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import com.voxelbuster.hardcorelivesplugin.event.PlayerDisplayLifeScoreboardEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class SubcommandScoreboard extends PluginSubcommand {
    protected SubcommandScoreboard(HardcoreLivesPlugin plugin) {
        super("scoreboard", plugin);
        this.description = "Toggle the scoreboard for yourself.";
        this.usage = "/hl scoreboard";
        this.aliases = Arrays.asList("scoreboard", "scoreboarddisplay", "sd");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "hardcorelives.scoreboard")) {
            if (aliases.contains(alias.toLowerCase())) {
                if (!(sender instanceof Player p)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player.");
                    return true;
                }

                if (p.getScoreboard() != plugin.getScoreboard()) {
                    sender.sendMessage(ChatColor.GOLD + "Lives scoreboard on.");
                    p.setScoreboard(plugin.getScoreboard());
                    plugin.getServer().getPluginManager().callEvent(new PlayerDisplayLifeScoreboardEvent(p));
                } else {
                    sender.sendMessage(ChatColor.GOLD + "Lives scoreboard off.");
                    p.setScoreboard(Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard());
                }
                return true;
            }
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return new ArrayList<>();
    }
}
