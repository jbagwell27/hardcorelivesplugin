package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.bukkit.ChatColor.*;

public class SubcommandHelp extends PluginSubcommand {
    protected SubcommandHelp(HardcoreLivesPlugin plugin) {
        super("help", plugin);
        this.description = "Show this help message.";
        this.usage = "/hl help" + YELLOW + " [subcommand]";
        this.aliases = Arrays.asList("help", "h");
    }

    @Override
    public boolean execute(CommandSender commandSender, String alias, String[] args) {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length < 1) {
                commandSender.sendMessage(new String[]{
                        AQUA + "/hl lives",
                        AQUA + "/hl scoreboard",
                        AQUA + "/hl lives buy " + GREEN + "<number>" +
                                ((plugin.getConfigManager().getConfig()
                                        .allowBuyingLives()) ? "" : RED + " : DISABLED"),
                        AQUA + "/hl lives sell " + GREEN + "<number>" +
                                ((plugin.getConfigManager().getConfig()
                                        .allowSellingLives()) ? "" : RED + " : DISABLED"),
                        AQUA + "/hl lives give " + GREEN + "<player> <number>" +
                                ((plugin.getConfigManager().getConfig()
                                        .allowGivingLives()) ? "" : RED + " : DISABLED"),
                        AQUA + "/hl scoreboardSlot " + GREEN + "<SIDEBAR/PLAYER_LIST/BELOW_NAME>" +
                                WHITE + " : Admin Only",
                        AQUA + "/hl lives " + GREEN + "<player>" + WHITE + " : Admin Only",
                        AQUA + "/hl resetAll " + WHITE + " : Admin Only",
                        AQUA + "/hl reset " + GREEN + "<player>" + WHITE + " : Admin Only",
                        AQUA + "/hl reload " + YELLOW + "[save_config (default=false)]" + WHITE + " : Admin Only",
                        AQUA + "/hl save " + WHITE + " : Admin Only",
                        AQUA + "/hl autosave " + GREEN + "<number> <time_interval>" + WHITE + " : Admin Only",
                        AQUA + "/hl config " + GREEN + "<key> <value>" + WHITE + " : Admin Only",
                        AQUA + "/hl setLives " + GREEN + "<player> <lives>" + WHITE + " : Admin Only",
                        });
            } else {
                PluginBaseCommand.SubCommand subCommandType = PluginBaseCommand.SubCommand.subCommandByAlias(args[0]);
                if (subCommandType == null) {
                    commandSender.sendMessage(RED + "No such subcommand.");
                    return false;
                }

                StringBuilder aliasesSb = new StringBuilder();
                Iterator<String> aliasesIterator = aliases.iterator();
                while (aliasesIterator.hasNext()) {
                    aliasesSb.append(GREEN).append(aliasesIterator.next()).append(GOLD).append(", ");
                }
                aliasesSb.append(aliasesIterator.next());

                PluginSubcommand subcommand;
                try {
                    subcommand = subCommandType.getSubcommand().getConstructor(HardcoreLivesPlugin.class)
                            .newInstance(plugin);
                    commandSender.sendMessage(GOLD + "Help for " + GREEN + subcommand.name);
                    commandSender.sendMessage(WHITE + subcommand.description);
                    commandSender.sendMessage(GOLD + "Aliases: " + aliasesSb);
                    commandSender.sendMessage(GOLD + "Usage: " + GREEN + subcommand.usage);
                } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                    plugin.getLogger().severe("Failed to reflectively construct subcommands!");
                    e.printStackTrace();
                }
                return true;
            }
        }
        sendUsageMessage(commandSender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length > 0) {
                return Arrays.asList((String[]) PluginBaseCommand.SubCommand.getAllAliases().stream()
                        .filter(s -> s.startsWith(args[0].toLowerCase())).toArray());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
