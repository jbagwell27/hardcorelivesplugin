package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SubcommandReset extends PluginSubcommand {
    protected SubcommandReset(HardcoreLivesPlugin plugin) {
        super("resetAll", plugin);
        this.description = "Resets the player data for a single player.";
        this.usage = "/hl reset <player>";
        this.aliases = Collections.singletonList("reset");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length == 1) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.reset")) {
                String playerName = args[0];
                Player p = plugin.getServer().getPlayer(playerName);
                if (p != null) {
                    configManager.setPlayerData(p, new ConfigManager.PlayerData(p, configManager.getConfig()));
                    sender.sendMessage(
                            ChatColor.LIGHT_PURPLE + "Hardcore Lives player " + p.getName() + " data wiped.");
                } else {
                    sender.sendMessage(ChatColor.RED + "Player not found.");
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        }
        sendUsageMessage(sender);
        return false;
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> playerNames = new ArrayList<>();
                plugin.getServer().getOnlinePlayers().forEach(p -> playerNames.add(p.getName()));
                return playerNames.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
