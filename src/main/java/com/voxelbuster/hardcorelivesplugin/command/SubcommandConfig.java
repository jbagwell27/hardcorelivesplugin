package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class SubcommandConfig extends PluginSubcommand {

    private final List<String> keyOptionNames = Arrays
            .asList("allowTotemOfUndying", "spectateWhenNoMoreLives", "startingLives",
                    "allowBuyingLives", "allowSellingLives", "allowGivingLives",
                    "lifeBuyPrice", "lifeSellPrice", "scoreboardDisplaySlot");

    protected SubcommandConfig(HardcoreLivesPlugin plugin) {
        super("config", plugin);
        this.description = "Set the value of a config variable. Takes effect immediately. " +
                "This will also overwrite the config! Changing the autosave configuration is not permitted with this" +
                "command. Use " + ChatColor.GREEN + "/hl autosave" + ChatColor.RESET + " instead.";
        this.usage = "/hl config <key> <value>";
        this.aliases = Arrays.asList("config", "c");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length == 2) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.config")) {
                try {
                    switch (args[0]) {
                        case "allowTotemOfUndying" ->
                                configManager.getConfig().setAllowTotemOfUndying(Boolean.parseBoolean(args[1]));
                        case "spectateWhenNoMoreLives" ->
                                configManager.getConfig().setSpectateWhenNoMoreLives(Boolean.parseBoolean(args[1]));
                        case "startingLives" -> configManager.getConfig().setStartingLives(Integer.parseInt(args[1]));
                        case "allowBuyingLives" ->
                                configManager.getConfig().setAllowBuyingLives(Boolean.parseBoolean(args[1]));
                        case "allowSellingLives" ->
                                configManager.getConfig().setAllowSellingLives(Boolean.parseBoolean(args[1]));
                        case "allowGivingLives" ->
                                configManager.getConfig().setAllowGivingLives(Boolean.parseBoolean(args[1]));
                        case "loseLifeOnPvpOnly" ->
                                configManager.getConfig().setLoseLifeOnPvpOnly(Boolean.parseBoolean(args[1]));
                        case "lifeBuyPrice" -> configManager.getConfig().setLifeBuyPrice(Float.parseFloat(args[1]));
                        case "lifeSellPrice" -> configManager.getConfig().setLifeSellPrice(Float.parseFloat(args[1]));
                        case "scoreboardDisplaySlot" ->
                                configManager.getConfig().setScoreboardDisplaySlot(DisplaySlot.valueOf(args[1]));
                        default -> sender.sendMessage(ChatColor.RED + "Invalid config option.");
                    }
                    plugin.getLogger().log(Level.INFO, "Saving config...");
                    try {
                        configManager.saveConfig(plugin.getConfigFile());
                    } catch (IOException e) {
                        plugin.getLogger().severe(ChatColor.RED + "Failed to save config!");
                        e.printStackTrace();
                    }
                    return true;
                } catch (RuntimeException e) {
                    sender.sendMessage(ChatColor.RED + "Invalid argument.");
                    return false;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else {
            sendUsageMessage(sender);
            return false;
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                return keyOptionNames.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
