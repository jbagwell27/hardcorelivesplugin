package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SubcommandGiveLives extends PluginSubcommand {
    protected SubcommandGiveLives(HardcoreLivesPlugin plugin) {
        super("giveLives", plugin);
        this.description = "Show the number of lives for yourself or another player.";
        this.usage = "/hl giveLives " + ChatColor.YELLOW + "[player (default: yourself)]";
        this.aliases = Arrays.asList("givelives", "l");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (aliases.contains(alias.toLowerCase())) {

            if (args.length == 2) {
                ConfigManager configManager = plugin.getConfigManager();
                if (!configManager.getConfig().allowGivingLives()) {
                    sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                    sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config values.");
                    return false;
                }

                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
                    return false;
                }

                if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.give")) {
                    Player p = (Player) sender;
                    Player target = Bukkit.getPlayer(args[0]);

                    ConfigManager.PlayerData data = configManager.getPlayerData(p);
                    ConfigManager.PlayerData dataTarget = configManager.getPlayerData(target);

                    if (target == null) {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                        return true;
                    }

                    int numLives = Integer.parseInt(args[1]);

                    if (numLives < data.getLives() && numLives > 0) {
                        data.setLives(data.getLives() - numLives);
                        plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        dataTarget.setLives(dataTarget.getLives() + numLives);
                        plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(target, dataTarget.getLives()));
                        p.sendMessage(
                                ChatColor.GREEN + String.format("Sent %d lives to %s.", numLives, target.getName()));
                        target.sendMessage(
                                ChatColor.GREEN + String.format("%s sent you %d extra lives.", p.getName(), numLives));
                        plugin.updateScoreboard();
                    } else {
                        sender.sendMessage("You cannot give more lives than you have or less than 1 life.");
                        return false;
                    }

                    if (data.getLives() == 1) {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                    } else {
                        p.sendMessage(
                                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                    }
                } else {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                    return false;
                }
                return true;
            }

        }
        sendUsageMessage(sender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> optionNames = new ArrayList<>();
                plugin.getServer().getOnlinePlayers().forEach(p -> optionNames.add(p.getName()));
                if (sender instanceof Player){
                    optionNames.remove(sender.getName());
                }
                return optionNames.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
