package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SubcommandAddLives extends PluginSubcommand {
    protected SubcommandAddLives(HardcoreLivesPlugin plugin) {
        super("addLives", plugin);
        this.description = "Add x amount of life to a single player.";
        this.usage = "/hl addLives <player> <number>";
        this.aliases = Arrays.asList("addlives", "al");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length == 2) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.setLives")) {

                if (args[0].equals("@a")) {

                    for (OfflinePlayer oP : configManager.getLoadedPlayers()) {
                        Player p = plugin.getServer().getPlayer(oP.getUniqueId());

                        ConfigManager.PlayerData data = configManager.getPlayerData(p);

                        data.setLives(data.getLives() + Integer.parseInt(args[1]));

                        plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        plugin.updateScoreboard();
                    }
                } else {

                    Player p = plugin.getServer().getPlayer(args[0]);
                    if (p == null) {
                        sender.sendMessage(ChatColor.RED + "Player not found.");
                    } else {
                        ConfigManager.PlayerData data = configManager.getPlayerData(p);
                        data.setLives(data.getLives() + Integer.parseInt(args[1]));
//                    data.setLives(Integer.parseInt(args[1]));
                        plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                        plugin.updateScoreboard();
                    }
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else {
            sendUsageMessage(sender);
            return false;
        }
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> playerNames = new ArrayList<>();
                plugin.getServer().getOnlinePlayers().forEach(p -> playerNames.add(p.getName()));
                playerNames.add("@a");
                return playerNames.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}
