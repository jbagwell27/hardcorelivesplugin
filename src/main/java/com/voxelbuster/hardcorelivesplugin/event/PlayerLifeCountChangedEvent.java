package com.voxelbuster.hardcorelivesplugin.event;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.jetbrains.annotations.NotNull;

public class PlayerLifeCountChangedEvent extends PlayerEvent {

    private final int lives;

    private static final HandlerList handlers = new HandlerList();

    public PlayerLifeCountChangedEvent(Player respawnPlayer, int livesAfterRespawn) {
        super(respawnPlayer);
        this.lives = livesAfterRespawn;
        if ((this.lives > 0) && (respawnPlayer.getGameMode() == GameMode.SPECTATOR)){
            respawnPlayer.setGameMode(GameMode.SURVIVAL);
        }
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

    public int getLives() {
        return lives;
    }
}
