package com.voxelbuster.hardcorelivesplugin.event;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.BanList;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityResurrectEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.io.File;
import java.io.IOException;

public class PluginEventHandler implements Listener {

    private final ConfigManager configManager;
    private final File playersDir;

    private final HardcoreLivesPlugin plugin;

    public PluginEventHandler(ConfigManager configManager, File playersDir,
                              HardcoreLivesPlugin plugin) {
        this.configManager = configManager;
        this.playersDir = playersDir;
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        try {
            configManager.loadPlayerData(p, playersDir);
            ConfigManager.PlayerData data = configManager.getPlayerData(p);
            if (!PermissionUtil.hasPermission(p, "harcorelives.bypass") && data.getLives() > 0) {
                checkLives(p, data);
                plugin.updateScoreboard();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void checkLives(Player p, ConfigManager.PlayerData data) {
        if (data.getLives() == 1) {
            p.sendMessage(ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
        } else {
            p.sendMessage(ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
        }

        if (data.getLives() <= 0 && !data.doesBypassLives() && !PermissionUtil
                .hasPermission(p, "hardcorelives.bypass")) {
            plugin.getServer().getPluginManager().callEvent(new PlayerOutOfLivesEvent(p));
            if (configManager.getConfig().canSpectateWhenNoMoreLives()) {
                p.sendMessage(ChatColor.AQUA + "You have no more lives. You can now spectate as a ghost.");
                p.setGameMode(GameMode.SPECTATOR);
            } else {
                plugin.getServer().getBanList(BanList.Type.NAME).addBan(p.getName(), "You have no more lives." +
                        "\nGame over man, game over!", null, "Hardcore Lives Plugin");
                p.kickPlayer("You have no more lives.\nGame over man, game over!");
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        ConfigManager.PlayerData data = configManager.getPlayerData(p);
        if (!PermissionUtil.hasPermission(p, "harcorelives.bypass") && data.getLives() > 0) {
            if (configManager.getConfig().doLoseLifeOnPvpOnly()) {
                if (p.getKiller() != null && p.getKiller() != p) {
                    data.setLives(data.getLives() - 1);
                }
            } else {
                data.setLives(data.getLives() - 1);
            }
            plugin.getServer().getPluginManager().callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        Player p = event.getPlayer();
        ConfigManager.PlayerData data = configManager.getPlayerData(p);
        if (!PermissionUtil.hasPermission(p, "harcorelives.bypass")) {
            checkLives(p, data);
            plugin.updateScoreboard();
        }
    }

    @EventHandler
    public void onRevive(EntityResurrectEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        if (!configManager.getConfig().allowsTotemOfUndying()) {
            event.getEntity().sendMessage(ChatColor.DARK_RED + "Nothing can save you from death.");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        try {
            plugin.updateScoreboard();
            configManager.unloadPlayer(p, playersDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
