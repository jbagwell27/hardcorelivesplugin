package com.voxelbuster.hardcorelivesplugin;

import com.voxelbuster.hardcorelivesplugin.command.PluginBaseCommand;
import com.voxelbuster.hardcorelivesplugin.event.PluginEventHandler;
import com.voxelbuster.hardcorelivesplugin.placeholders.HardcoreLivesExpansion;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public final class HardcoreLivesPlugin extends JavaPlugin {

    private static final HardcoreLivesPlugin instance;
    private static Economy econ = null;
    private static Permission perms = null;

    static {
        instance = (HardcoreLivesPlugin) Bukkit.getPluginManager().getPlugin("Hardcorelivesplugin");
        assert instance != null;
    }

    final Logger log = this.getLogger();

    private final File dataFolder = this.getDataFolder();
    private final File playersDir = new File(Paths.get(dataFolder.toPath().toString(), "players").toString());
    private final File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
    private final ConfigManager configManager = new ConfigManager(this);
    private final SaveTaskScheduler saveTaskScheduler = new SaveTaskScheduler(this);
    private final PluginEventHandler eventHandler = new PluginEventHandler(configManager, playersDir, this);
    private Objective scoreboardObjective;
    private boolean enabledBefore;
    private Scoreboard scoreboard;

    public static HardcoreLivesPlugin getInstance() {
        return instance;
    }

    public static Economy getEcon() {
        return econ;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {
        // Replaced by PluginBaseCommand.onCommand();
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        log.log(Level.INFO, "Saving config...");
        try {
            configManager.saveConfig(configFile);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save config!");
            e.printStackTrace();
        }
        log.log(Level.INFO, "Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save playerdata!");
            e.printStackTrace();
        }
        log.log(Level.INFO, "Hardcore Lives disabled.");
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public void onEnable() {
        log.log(Level.INFO, "Loading config...");

        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
            configManager.loadConfig(configFile);
            //noinspection ResultOfMethodCallIgnored
            playersDir.mkdir();
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Could not create data directory! Will use default config values!");
            e.printStackTrace();
        }

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                    "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                    "work.");
        }

        if (setupEconomy()) {
            log.info("Vault Economy API Hooked.");
        } else {
            log.warning("Economy not hooked, features requiring economy will not work!");
        }

        if (setupPermissions()) {
            PermissionUtil.setPermissionManager(perms);
            log.info("Permission manager hooked.");
        } else {
            log.warning("Permission manager not detected.");
        }

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new HardcoreLivesExpansion(this).register();
            log.info("PAPI hooked and HL expansion registered.");
        }

        PermissionUtil.registerPermissions();

        // Load online players (only does anything on reload)
        this.getServer().getOnlinePlayers().forEach(player -> {
            try {
                configManager.loadPlayerData(player, playersDir);
            } catch (IOException e) {
                log.severe(Arrays.stream(e.getStackTrace())
                        .map(StackTraceElement::toString)
                        .collect(Collectors.joining()));
            }
        });

        this.scoreboard = setupScoreboard();

        updateScoreboard();

        if (!enabledBefore) {
            this.getServer().getPluginManager().registerEvents(eventHandler, this);
        }

        PluginBaseCommand pbc = new PluginBaseCommand("hardcorelives", this);

        PluginCommand c = this.getCommand("hardcorelives");

        if (c == null) {
            log.severe("Cannot bind plugin command!");
            throw new IllegalStateException();
        }

        c.setExecutor(pbc);
        c.setTabCompleter(pbc);

        saveTaskScheduler.setSaveInterval(configManager.getConfig().getAutosaveUnit()
                .toMillis(configManager.getConfig().getAutosaveInterval()) / 50);
        saveTaskScheduler.rescheduleTask();

        this.enabledBefore = true;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return true;
    }

    private boolean setupPermissions() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            return false;
        }
        perms = rsp.getProvider();
        return true;
    }

    public void updateScoreboard() {
        Bukkit.getOnlinePlayers().forEach(this::updateScoreboard);
    }

    private Scoreboard setupScoreboard() {
        @SuppressWarnings("ConstantConditions")
        Scoreboard livesScoreboard = getServer().getScoreboardManager().getNewScoreboard();

        this.scoreboardObjective = livesScoreboard.registerNewObjective("lives", "dummy", "Lives");
        this.scoreboardObjective.setDisplayName("Lives");
        this.scoreboardObjective.setDisplaySlot(configManager.getConfig().getScoreboardDisplaySlot());

        return livesScoreboard;
    }

    public void updateScoreboard(Player toUpdate) {
        Score playerLivesScore = this.scoreboardObjective.getScore(toUpdate.getName());
        playerLivesScore.setScore(configManager.getPlayerData(toUpdate).getLives());
    }

    @SuppressWarnings("DuplicatedCode")
    public void reload(boolean saveConfig) {
        log.log(Level.INFO, "Reloading HardcoreLives...");

        if (saveConfig) {
            log.log(Level.INFO, "Saving config...");
            try {
                configManager.saveConfig(configFile);
            } catch (IOException e) {
                log.severe(ChatColor.RED + "Failed to save config!");
                e.printStackTrace();
            }
        }
        log.log(Level.INFO, "Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Failed to save playerdata!");
            e.printStackTrace();
        }

        log.log(Level.INFO, "Hardcore Lives disabled.");

        log.log(Level.INFO, "Loading config...");

        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
            configManager.loadConfig(configFile);
            //noinspection ResultOfMethodCallIgnored
            playersDir.mkdir();
        } catch (IOException e) {
            log.severe(ChatColor.RED + "Could not create data directory! Will use default config values!");
            e.printStackTrace();
        }

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                    "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                    "work.");
        }

        if (setupEconomy()) {
            log.info("Vault API Hooked.");
        } else {
            log.warning("Vault not found, features that require economy will not work.");
        }

        this.scoreboard = setupScoreboard();

        this.getServer().getOnlinePlayers().forEach(p -> {
            try {
                configManager.loadPlayerData(p, playersDir);
            } catch (IOException e) {
                log.severe(String.format(ChatColor.RED + "Failed to load player %s (UUID %s)!", p.getName(),
                        p.getUniqueId()));
                e.printStackTrace();
            }
        });

        updateScoreboard();
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public Objective getScoreboardObjective() {
        return scoreboardObjective;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public SaveTaskScheduler getSaveTaskScheduler() {
        return saveTaskScheduler;
    }

    public File getPlayersDir() {
        return playersDir;
    }

    public File getConfigFile() {
        return configFile;
    }
}
